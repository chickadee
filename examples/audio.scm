(use-modules (chickadee)
             (chickadee audio)
             (chickadee math vector)
             (chickadee graphics text)
             (ice-9 match))

(define effect #f)
(define music #f)
(define music-source #f)
(define playlist
  #("audio/the-forgotten-land.mp3"
    "audio/venus.wav"
    "audio/spooky-dungeon.ogg"))
(define playlist-index -1)
(define status-message "")
(define status-position (vec2 200.0 240.0))
(define instructions
  "VAMP (Very Advanced Music Player)

CONTROLS:

SPACE - play/pause
N - next music file
P - previous music file
L - play an explosion sound just because
Q - quit")
(define instructions-position (vec2 200.0 400.0))

(define (playlist-move n)
  (source-stop music-source)
  (set! playlist-index (modulo (+ playlist-index n) (vector-length playlist)))
  (let ((file-name (vector-ref playlist playlist-index)))
    (set! music (load-audio file-name #:mode 'stream))
    (set-source-audio! music-source music)
    (source-play music-source)
    (set! status-message (string-append "NOW PLAYING: " (basename file-name)))))

(define (load)
  (set! effect (load-audio "audio/explosion.wav"))
  (set! music-source (make-source))
  (playlist-move 1))

(define (update dt)
  (when (source-stopped? music-source)
    (playlist-move 1)))

(define (draw alpha)
  (when (>= playlist-index 0)
    (draw-text status-message status-position))
  (draw-text instructions instructions-position))

(define (key-press key modifiers repeat?)
  (match key
    ('q (abort-game))
    ('space (source-toggle music-source))
    ('l (audio-play effect))
    ('n
     (playlist-move 1))
    ('p
     (playlist-move -1))
    (_ #t)))

(run-game #:load load #:update update #:key-press key-press #:draw draw)
