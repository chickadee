;;; Chickadee Game Toolkit
;;; Copyright © 2023 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (tests base64)
  #:use-module (chickadee base64)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-64)
  #:use-module (tests utils))

(with-tests "base64"
  (test-group "base64-decode"
    (test-equal "empty string"
      ""
      (utf8->string (base64-decode "")))
    (test-equal "no padding"
      "hello!"
      (utf8->string (base64-decode "aGVsbG8h")))
    (test-equal "one byte of padding"
      "hello"
      (utf8->string (base64-decode "aGVsbG8=")))
    (test-equal "one byte of padding but no padding char"
      "hello"
      (utf8->string (base64-decode "aGVsbG8" #:padding? #f)))
    (test-equal "two bytes of padding"
      "what's up?"
      (utf8->string (base64-decode "d2hhdCdzIHVwPw==")))
    (test-equal "two bytes of padding but no padding chars"
      "what's up?"
      (utf8->string (base64-decode "d2hhdCdzIHVwPw" #:padding? #f))))
  (test-group "base64-encode"
    (test-equal "empty string"
      ""
      (base64-encode (string->utf8 "")))
    (test-equal "no padding"
      "aGVsbG8h"
      (base64-encode (string->utf8 "hello!")))
    (test-equal "one byte of padding"
      "aGVsbG8="
      (base64-encode (string->utf8 "hello")))
    (test-equal "one byte of padding but no padding char"
      "aGVsbG8"
      (base64-encode (string->utf8 "hello") #:padding? #f))
    (test-equal "two bytes of padding but no padding chars"
      "d2hhdCdzIHVwPw"
      (base64-encode (string->utf8 "what's up?") #:padding? #f))))
