;;; Chickadee Game Toolkit
;;; Copyright © 2016, 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (chickadee graphics blend)
  #:use-module (chickadee graphics engine)
  #:use-module (chickadee graphics gl)
  #:use-module (gl)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:export (make-blend-mode
            blend-mode?
            blend-mode-equation
            blend-mode-source-function
            blend-mode-destination-function
            blend:alpha
            blend:multiply
            blend:subtract
            blend:add
            blend:lighten
            blend:darken
            blend:screen
            blend:replace
            g:blend-mode
            current-blend-mode))

(define-record-type <blend-mode>
  (make-blend-mode equation source-function destination-function)
  blend-mode?
  (equation blend-mode-equation)
  (source-function blend-mode-source-function)
  (destination-function blend-mode-destination-function))

(define blend:alpha
  (make-blend-mode 'add 'source-alpha 'one-minus-source-alpha))
(define blend:multiply
  (make-blend-mode 'add 'destination-color 'zero))
(define blend:subtract
  (make-blend-mode 'reverse-subtract 'one 'zero))
(define blend:add
  (make-blend-mode 'add 'one 'one))
(define blend:lighten
  (make-blend-mode 'max 'one 'zero))
(define blend:darken
  (make-blend-mode 'min 'one 'zero))
(define blend:screen
  (make-blend-mode 'add 'one 'one-minus-source-color))
(define blend:replace
  (make-blend-mode 'add 'one 'zero))

(define (bind-blend-mode blend-mode)
  (if blend-mode
      (begin
        (gl-enable (enable-cap blend))
        (gl-blend-equation
         (match (blend-mode-equation blend-mode)
           ('add
            (blend-equation-mode-ext func-add-ext))
           ('subtract
            (blend-equation-mode-ext func-subtract-ext))
           ('reverse-subtract
            (blend-equation-mode-ext func-reverse-subtract-ext))
           ('min
            (blend-equation-mode-ext min-ext))
           ('max
            (blend-equation-mode-ext max-ext))
           ('alpha-min
            (blend-equation-mode-ext alpha-min-sgix))
           ('alpha-max
            (blend-equation-mode-ext alpha-min-sgix))))
        (gl-blend-func (match (blend-mode-source-function blend-mode)
                         ('zero
                          (blending-factor-src zero))
                         ('one
                          (blending-factor-src one))
                         ('destination-color
                          (blending-factor-src dst-color))
                         ('one-minus-destination-color
                          (blending-factor-src one-minus-dst-color))
                         ('source-alpha-saturate
                          (blending-factor-src src-alpha-saturate))
                         ('source-alpha
                          (blending-factor-src src-alpha))
                         ('one-minus-source-alpha
                          (blending-factor-src one-minus-src-alpha))
                         ('destination-alpha
                          (blending-factor-src dst-alpha))
                         ('one-minus-destination-alpha
                          (blending-factor-src one-minus-dst-alpha))
                         ('constant-color
                          (blending-factor-src constant-color-ext))
                         ('one-minus-constant-color
                          (blending-factor-src one-minus-constant-color-ext))
                         ('contstant-alpha
                          (blending-factor-src constant-alpha-ext))
                         ('one-minus-constant-alpha
                          (blending-factor-src one-minus-constant-alpha-ext)))
                       (match (blend-mode-destination-function blend-mode)
                         ('zero
                          (blending-factor-dest zero))
                         ('one
                          (blending-factor-dest one))
                         ('source-color
                          (blending-factor-dest src-color))
                         ('one-minus-source-color
                          (blending-factor-dest one-minus-src-color))
                         ('source-alpha
                          (blending-factor-dest src-alpha))
                         ('one-minus-source-alpha
                          (blending-factor-dest one-minus-src-alpha))
                         ('destination-alpha
                          (blending-factor-dest dst-alpha))
                         ('one-minus-destination-alpha
                          (blending-factor-dest one-minus-dst-alpha))
                         ('constant-color
                          (blending-factor-dest constant-color-ext))
                         ('one-minus-constant-color
                          (blending-factor-dest one-minus-constant-color-ext))
                         ('contstant-alpha
                          (blending-factor-dest constant-alpha-ext))
                         ('one-minus-constant-alpha
                          (blending-factor-dest one-minus-constant-alpha-ext)))))
      (gl-disable (enable-cap blend))))

(define-graphics-state g:blend-mode
  current-blend-mode
  #:default blend:replace
  #:bind bind-blend-mode)
