;;; Chickadee Game Toolkit
;;; Copyright © 2021 David Thompson <dthompson2@worcester.edu>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (tests vector)
  #:use-module (tests utils)
  #:use-module (srfi srfi-64)
  #:use-module (chickadee math)
  #:use-module (chickadee math vector))

(with-tests "vector"
  (test-group "2D vectors"
    (test-assert "vec2="
      (vec2= (vec2 1.0 2.0) (vec2 1.0 2.0)))
    (test-group "vec2/polar"
      (test-equal (vec2/polar (vec2 1.0 1.0) 2.0 0.0) (vec2 3.0 1.0))
      (test-equal (vec2/polar (vec2 1.0 1.0) 2.0 (/ pi 2)) (vec2 1.0 3.0))
      (test-equal (vec2/polar (vec2 1.0 1.0) 2.0 pi) (vec2 -1.0 1.0))
      (test-equal (vec2/polar (vec2 1.0 1.0) 2.0 (* pi 1.5)) (vec2 1.0 -1.0))
      (test-equal (vec2/polar (vec2 1.0 1.0) 2.0 tau) (vec2 3.0 1.0)))
    (test-group "vec2-magnitude"
      (test-equal "normal case"
        (vec2-magnitude (vec2 -3.0 4.0)) 5.0)
      (test-equal "division by zero edge case"
        (vec2-magnitude (vec2 0.0 0.0)) 0.0))
    (test-equal "vec2-dot"
      (vec2-dot (vec2 1.0 2.0) (vec2 3.0 4.0)) 11.0)
    (test-group "vec2-cross"
      (test-equal "same line"
        (vec2-cross (vec2 1.0 2.0) (vec2 2.0 4.0)) 0.0)
      (test-equal "clockwise"
        (vec2-cross (vec2 1.0 2.0) (vec2 0.0 2.0)) 2.0)
      (test-equal "counter-clockwise"
        (vec2-cross (vec2 1.0 2.0) (vec2 2.0 0.0)) -4.0))
    (test-approximate "vec2-normalize"
      (vec2-magnitude (vec2-normalize (vec2 3.7 4.9))) 1.0 0.001)
    (test-group "vec2*"
      (test-equal "vec2 * vec2"
        (vec2* (vec2 2.0 3.0) (vec2 4.0 5.0)) (vec2 8.0 15.0))
      (test-equal "vec2 * scalar"
        (vec2* (vec2 2.0 3.0) 4.0) (vec2 8.0 12.0)))
    (test-group "vec2+"
      (test-equal "vec2 + vec2"
        (vec2+ (vec2 1.0 2.0) (vec2 3.0 4.0)) (vec2 4.0 6.0))
      (test-equal "vec2 + scalar"
        (vec2+ (vec2 1.0 2.0) 3.0) (vec2 4.0 5.0)))
    (test-group "vec2-"
      (test-equal "vec2 - vec2"
        (vec2- (vec2 1.0 2.0) (vec2 3.0 4.0)) (vec2 -2.0 -2.0))
      (test-equal "vec2 - scalar"
        (vec2- (vec2 1.0 2.0) 3.0) (vec2 -2.0 -1.0))))
  (test-group "3D vectors"
    (test-assert "vec3="
      (vec3= (vec3 1.0 2.0 3.0) (vec3 1.0 2.0 3.0)))
    (test-group "vec3-magnitude"
      (test-equal "normal case"
        (vec3-magnitude (vec3 -2.0 3.0 -6.0)) 7.0)
      (test-equal "division by zero edge case"
        (vec3-magnitude (vec3 0.0 0.0 0.0)) 0.0))
    (test-equal "vec3-dot"
      (vec3-dot (vec3 1.0 2.0 3.0) (vec3 4.0 5.0 6.0)) 32.0)
    (test-equal "vec3-cross"
      (vec3-cross (vec3 2.0 3.0 4.0) (vec3 5.0 6.0 7.0)) (vec3 -3.0 6.0 -3.0))
    (test-approximate "vec3-normalize"
      (vec3-magnitude (vec3-normalize (vec3 3.7 4.9 9.2))) 1.0 0.001)
    (test-group "vec3*"
      (test-equal "vec3 * vec3"
        (vec3* (vec3 2.0 3.0 4.0) (vec3 5.0 6.0 7.0)) (vec3 10.0 18.0 28.0))
      (test-equal "vec3 * scalar"
        (vec3* (vec3 2.0 3.0 4.0) 5.0) (vec3 10.0 15.0 20.0)))
    (test-group "vec3+"
      (test-equal "vec3 + vec3"
        (vec3+ (vec3 1.0 2.0 3.0) (vec3 4.0 5.0 6.0)) (vec3 5.0 7.0 9.0))
      (test-equal "vec3 + scalar"
        (vec3+ (vec3 1.0 2.0 3.0) 4.0) (vec3 5.0 6.0 7.0)))
    (test-group "vec3-"
      (test-equal "vec3 - vec3"
        (vec3- (vec3 1.0 2.0 3.0) (vec3 4.0 5.0 6.0)) (vec3 -3.0 -3.0 -3.0))
      (test-equal "vec3 - scalar"
        (vec3- (vec3 1.0 2.0 3.0) 4.0) (vec3 -3.0 -2.0 -1.0)))))
