(use-modules (chickadee)
             (chickadee math)
             (chickadee math matrix)
             (chickadee math vector)
             (chickadee graphics engine)
             (chickadee graphics model)
             (chickadee graphics text)
             (ice-9 format))

(define projection (perspective-projection (/ pi 3) (/ 4.0 3.0) 0.1 500.0))
(define translation (make-identity-matrix4))
(define view-matrix (make-identity-matrix4))
(define model-matrix (make-identity-matrix4))
(define position (vec3 0.0 0.0 0.0))
(define velocity (vec3 0.0 0.0 0.0))
(define model #f)
(define text-position (vec2 4.0 464.0))
(define text "")
(define y-rotation 0.0)
(define rotate-paused? #f)

(define (reset-position)
  (set-vec3! position 0.0 0.0 -4.0))

(define (load)
  (set! model (load-gltf "models/Suzanne/Suzanne.gltf"))
  (reset-position))

(define (draw alpha)
  (with-projection projection
    (draw-model model
                #:model-matrix model-matrix
                #:view-matrix view-matrix
                #:camera-position position))
  (draw-text text text-position))

(define (update dt)
  ;; Update camera position
  (set-vec3! velocity
             (+ (if (key-pressed? 'd) -1.0 0.0)
                (if (key-pressed? 'a) 1.0 0.0))
             0.0
             (+ (if (key-pressed? 's) -1.0 0.0)
                (if (key-pressed? 'w) 1.0 0.0)))
  (vec3-normalize! velocity)
  (vec3-mult! velocity 0.1)
  (vec3-add! position velocity)
  (set! text (format #f "(~6,2f, ~6,2f, ~6,2f)"
                     (vec3-x position)
                     (vec3-y position)
                     (vec3-z position)))

  ;; Compute new view matrix
  (look-at! view-matrix
          position
          (vec3+ position (vec3 0.0 0.0 1.0))
          (vec3 0.0 1.0 0.0))

  ;; Rotate the model about the y-axis
  (unless rotate-paused?
    (set! y-rotation (+ y-rotation 0.03))
    (matrix4-rotate-y! model-matrix y-rotation)))

(define (key-press key modifiers repeat?)
  (case key
   ((q)
    (abort-game))
   ((r)
    (reset-position))
   ((space)
    (set! rotate-paused? (not rotate-paused?)))))

(run-game #:window-title "3D!"
          #:load load
          #:draw draw
          #:update update
          #:key-press key-press)
