// -*- mode: c -*-
#extension GL_NV_shadow_samplers_cube : enable

#ifdef GLSL120
varying vec3 fragDir;
#else
in vec3 fragDir;
#endif

#ifdef GLSL330
out vec4 fragColor;
#endif

#ifndef GLSL330
#define fragColor gl_FragColor
#endif

uniform samplerCube cubeMap;

#ifndef GLSL330
// Compatibility shim for older GLSL versions.
vec3 texture(samplerCube tex, vec3 coord) {
  return textureCube(tex, coord);
}
#endif

void main() {
  fragColor = texture(cubeMap, fragDir);
}
