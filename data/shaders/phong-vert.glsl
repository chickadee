// -*- mode: c -*-

#ifdef GLSL330
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;
layout (location = 2) in vec3 normal;
#elif defined(GLSL130)
in vec3 position;
in vec2 texcoord;
in vec3 normal;
#elif defined(GLSL120)
attribute vec3 position;
attribute vec2 texcoord;
attribute vec3 normal;
#endif

#ifdef GLSL120
varying vec3 fragWorldPos;
varying vec3 fragNorm;
varying vec2 fragTex;
#else
out vec3 fragWorldPos;
out vec3 fragNorm;
out vec2 fragTex;
#endif

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
  fragWorldPos = vec3(model * vec4(position, 1.0));
  fragNorm = mat3(model) * normal;
  fragTex = texcoord;
  gl_Position = projection * view * vec4(fragWorldPos, 1.0);
}
